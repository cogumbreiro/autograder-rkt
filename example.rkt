#lang racket
(require rackunit)
(require json)
(require autograder)

(define all-tests
  (test-suite
    "Examples"
    (test-case
      "Example 1"
       (check-equal? 1 2))
    ;; -----------
    (test-case
      "Example 2"
      (check-equal? 2 2))))

; A weight is eihter a real, or a pair (real, visibility)
; Where visibility is one of: 'visible, 'hidden, 'after-due-date, 'after-published
(define weights (hash
                 "Example 1" 1.0 ; defaults to visible
                 "Example 2" (cons 3.0 'hidden)))

(display (jsexpr->string (batch->json (autograde-tests weights all-tests))))

(newline)
