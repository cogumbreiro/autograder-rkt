# Autograder

Autograder runs a test-suite and it produces a Gradescope-compatible JSON
object, which can then be rendered.

The user must supply a score map (as per [Gradescope's
spec](https://gradescope-autograders.readthedocs.io/en/latest/specs/)). The keys (strings)
correspond to the test identifier (and should match the string identifier of the given `test-case`). The values of a score map are the points and optional visibility, so
either a `real?` (the points) or a pair of points (`real?`) and visibility (`visibility?`). When only given a score, the visibility defaults to `'visible`.

A visibility is one of: `'visible`, `'hidden`, `'after-due-date`,
`'after-published`.


```scheme
#lang racket
(require rackunit)
(require json)
(require autograder)

(define all-tests
  (test-suite
    "Examples"
    (test-case
      "Example 1"
       (check-equal? 1 2))
    ;; -----------
    (test-case
      "Example 2"
      (check-equal? 2 2))))

(define weights (hash
                 "Example 1" 1.0 ; defaults to visible
                 "Example 2" (cons 3.0 'hidden)))

(display (jsexpr->string (batch->json (autograde-tests weights all-tests))))

(newline)
```

Let's save the above script as `example.rkt` and run it.
The output is as follows (pretty-printed by me):
```bash
$ racket example.rkt
{
   "execution_time" : 0,
   "tests" : [
      {
         "visibility" : "visible",
         "max_score" : 1,
         "score" : 0,
         "output" : "--------------------\nExample 1\nFAILURE\nname:       check-equal?\nlocation:   example.rkt:11:7\nactual:     1\nexpected:   2\n--------------------\n",
         "name" : "Example 1"
      },
      {
         "max_score" : 3,
         "visibility" : "hidden",
         "name" : "Example 2",
         "output" : "",
         "score" : 3
      }
   ]
}
```

